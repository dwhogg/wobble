"""
This file is part of the Wobble project.
Copyright 2017 The authors.

# precision.py

The main idea behind this code is to convert a tophat mask into a
synthetic spectrum, under different assumptions about how accurate the
tophat mask is. Then make fake data from the synthetic spectrum.  Then
compare cross-correlation with the mask to cross-correlation with the
synthetic spectrum to determine how much information is being lost by
using the mask and not a synthetic spectrum. Don't read too much into
this; this is only part of many, nested stories.

## bugs
- Not yet written.
"""
import os as os
import pickle as cp
import numpy as np

def write_pickle(fn, stuff):
    fd = open(fn, "wb")
    cp.dump(stuff, fd)
    print("writing", fn)
    fd.close()

def read_pickle(fn):
    fd = open(fn, "rb")
    stuff = cp.load(fd)
    fd.close()
    return stuff

def make_wavelength_grid():
    dwave = 0.01 # Angstrom
    w1, w2 = 3750, 6800
    return np.arange(w1 + 0.5 * dwave, w2, dwave)

def read_mask(fn="data/G2.mas"):
    w1, w2, ww = np.loadtxt(fn, unpack=True, dtype=np.float64)
    mask = np.vstack((w1, w2, ww)).T
    return mask

def oned_gaussian(xs, mu, sigma):
    deltas = (xs - mu) / sigma
    return np.exp(-0.5 * deltas * deltas) / np.sqrt(2 * np.pi * sigma * sigma)

def doppler(v):
    beta = v / 299792458. # m/s
    return np.sqrt((1. + beta) / (1 - beta))

def make_synthetic_spectrum(waves, mask, v=0.0):
    lsfsigma = 0.040 # Angstrom
    dd = doppler(v)
    print("starting to build synthetic spectrum...")
    synths = np.ones_like(waves)
    for i in range(len(mask[:,0])):
        wa = 0.5       * (mask[i, 1] + mask[i, 0]) * dd
        ew = mask[i,2] * (mask[i, 1] - mask[i, 0])
        synths -= ew * oned_gaussian(waves, wa, lsfsigma)
    print("...returning synthetic spectrum")
    return synths

def make_data(synths):
    return data, ivar

if __name__ == "__main__":
    from matplotlib import pylab as plt

    mask = read_mask()

    # make fake data and derivatives (should be a function!)
    synthfn = "synth.pickle"
    if os.path.exists(synthfn):
        waves, synths, dsynthdvs = read_pickle(synthfn)
    else:
        waves = make_wavelength_grid()
        synths = make_synthetic_spectrum(waves, mask)
        dv = 30.
        synths2 = make_synthetic_spectrum(waves, mask, v=dv)
        dsynthdvs = (synths2 - synths) / dv
        write_pickle(synthfn, (waves, synths, dsynthdvs))

    ivars = 1.e4 + np.zeros_like(waves)
    crb = 1. / np.sqrt(np.sum(dsynthdvs * ivars * dsynthdvs))
    print("The CRB in m/s is", crb)

    plt.clf()
    plt.step(waves, synths, "k-")
    plt.step(waves, 3000. * dsynthdvs, "r-")
    plt.xlim(3750, 3751)
    plt.savefig("precision.png")
